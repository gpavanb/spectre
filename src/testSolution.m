% SOLVE 1D
% VARIABLE COEFFICIENT
% EVEN ORDER DIFFERENTIAL EQUATION
% USING JACOBI/LAGUERRE POLYNOMIALS
% NUMERICALLY
% AT THE LEGENDRE-GAUSS-LOBATTO/LAGUERRE-GAUSS-RADAU POINTS
% AUTHOR : G. PAVAN BHARADWAJ

% CLEAN
clc; clearvars; close all;

% ADD PATHS
addpath('../include/','../include/jacobi/','../include/legendre/', ...
    '../include/laguerre');

global dom;

%% INPUTS

% FINITE OR SEMI-INFINITE DOMAINS
% 0 FOR FINITE, 1 FOR SEMI-INFINITE
dom = 1;

% INPUT EQUATION
% NOTE : USE ONLY EVEN ORDER
ord = 4;

% EQUATION PARAMETERS
% RHS

% SECOND ORDER - FINITE
% if (dom == 0)
%     rhs = @(x) sin(2*pi*x);
% end

% SECOND-ORDER EXAMPLE - FINITE
% p = 1.05;
% rhs = @(x) -3*p*x./( ((p + x.^2).^2)*sqrt(p+1) );

% FOURTH ORDER EXAMPLE - FINITE
% rhs = @(x) 36 - 20*x.^2;

% SECOND ORDER EXAMPLE - SEMI_INFINITE
% if (dom == 1)
%     rhs = @(x) -2*exp(-x).*cos(x);
% end

% FOURTH ORDER EXAMPLE - SEMI_INFINITE
if (dom == 1)
    rhs = @(x) 0.5*exp(-x).*(1+7*cos(2*x)+24*sin(2*x));
end

% SIXTH ORDER EXAMPLE - SEMI_INFINITE
% if (dom == 1)
%     rhs = @(x) exp(-x).*(6*cos(x)+234*cos(3*x)-88*sin(3*x));
% end

% EIGHTH ORDER EXAMPLE - SEMI_INFINITE
% if (dom == 1)
%     rhs = @(x) 0.125*exp(-x).*(-3-2108*cos(2*x)+31679*cos(4*x)+1344*sin(2*x)+77280*sin(4*x));
% end

%%  EXACT SOLUTION

% % SECOND ORDER EXAMPLE - FINITE
% if (dom == 0)
%     u_exact = @(x) -(1/(4*pi*pi))*sin(2*pi*x);
% end

% FOURTH ORDER EXAMPLE - FINITE
% if (dom == 0)
%     u_exact = @(x) (1/(16*(pi.^4)))*(pi*(x-x.^3) + sin(2*pi*x));
% end

% SIXTH ORDER EXAMPLE - FINITE
% if (dom == 0)
%     u_exact = @(x) -(pi*x.*(7 - 10*x.^2 + 3*x.^4) + 4*sin(2*pi*x))/(256*(pi.^6));
% end

% EIGHTH ORDER EXAMPLE - FINITE
% if (dom == 0)
%    u_exact = @(x)  (pi*x.*(-1 + x.^2).*(-57 + 48*x.^2 - 15*x.^4 + ... 
%     4*(pi.^2).*(-1 + x.^2).^2) + 24*sin(2*pi*x))/(6144*(pi.^8));
% end

% SECOND-ORDER EXAMPLE - FINITE
% u_exact = @(x) -x./sqrt(p+1) + x./sqrt(p+x.^2);

% FOURTH ORDER EXAMPLE - FINITE
% u_exact = @(x) x.^4 - 2*x.^2 + 1;

% SECOND ORDER EXAMPLE - SEMI_INFINITE
% if (dom == 1)
%     u_exact = @(x) exp(-x).*sin(x);
% end

% FOURTH ORDER EXAMPLE - SEMI_INFINITE
if (dom == 1)
    u_exact = @(x) exp(-x).*(sin(x).^2);
end

% % SIXTH ORDER EXAMPLE - SEMI_INFINITE
% if (dom == 1)
%     u_exact = @(x) exp(-x).*(sin(x).^3);
% end

% % EIGHTH ORDER EXAMPLE - SEMI_INFINITE
% if (dom == 1)
%     u_exact = @(x) exp(-x).*(sin(x).^4);
% end

% MATRIX SIZE
N = 80;

% NUMBER OF QUADRATURE POINTS
Ngrid = 2*N;

%% COMPUTE

% ORDER OF EQUATION
assert(mod(ord,2) == 0);
m = ord/2;
alp_g = -m;
bet_g = -m;

% GENERATE GRID
if (dom == 0)
    n0 = computeN0(alp_g,bet_g);
    Ngrid_eff = Ngrid - n0;
    [xx,ww] = legslb(Ngrid_eff);
elseif (dom == 1)
    Ngrid_eff = Ngrid;
    [xx,w,ww] = lagsrd(Ngrid_eff);
end

% COMPUTE BASIS FUNCTIONS AND SCALING FACTORS
if (dom == 0)
    phi = zeros(N-2*m+1,Ngrid_eff);
    sca = zeros(size(phi,1),m);
    
    for i = 1:size(phi,1)
        j = i-1;
        % phi(i,:) = lepoly(j,xx) + (-(4*j+10)*lepoly(j+2,xx) + (2*j+3)*lepoly(j+4,xx))/(2*j+7);
        phi(i,:) = gjapoly(j+2*m,-m,-m,xx);
    end
    
    for i = 1:size(phi,1)
        j = i-1;
        % sca(i) =  sqrt( 1./( 2*((2*j+3).^2).*(2*j+5) ) );
        sca(i) = 1./sqrt(dot(ww,japolyderm(j+2*m,-m,-m,m,xx).^2));
    end
    
    for i = 1:size(phi,1)
        phi(i,:) = sca(i)*phi(i,:);
    end
    
elseif (dom == 1)
    % COMPUTE SCHEME COEFFICIENTS
    phi = zeros(N,Ngrid_eff);
    
    for i = 1:size(phi,1)
        j = i-1;
        c1 = -2;
        c2 = 1;
        % c3 = -1;
        % c4 = 1;
        % phi(i,:) = lafun(j,xx) + c1*lafun(j+1,xx);
        phi(i,:) = lafun(j,xx) + c1*lafun(j+1,xx) + c2*lafun(j+2,xx) ;% + c3*lafun(j+3,xx); % + c4*lafun(j+4,xx);
    end
end

% COMPUTE TRANSFORMED RHS
f = zeros(size(phi,1),1);

for i = 1:length(f)
    f(i) =  dot(ww,rhs(xx)'.*phi(i,:));
end

% CREATE GALERKIN MATRIX
aMat = zeros(ord+1,length(xx));
aMat(1,:) = 1;
% aMat(3,:) = 3*p./((p + xx.^2).^2);
% aMat(3,:) = -xx.^2;
% aMat(4,:) = 0;
% aMat(5,:) = 12;

% COMPUTE GALERKIN MATRIX
A = zeros(size(phi,1),size(phi,1));
for i = 1:size(phi,1)
    for j = max(i-m,1):min(i+m,size(phi,1))
        A(i,j) = bil(i,j,m,xx,ww,aMat,phi);
    end
end

% FILTER A
A(abs(A)< 1e-10) = 0;

% SOLVE FOR U
u = A\f;

% COMPUTE SOLUTION IN PHYSICAL SPACE
u_num = zeros(1,Ngrid_eff);
for i = 1:size(phi,1)
    u_num = u_num + u(i)*phi(i,:);
end

% FIND NORM ERROR - L1
err = dot(abs(u_num-u_exact(xx)'),ww)

% PLOT SOLUTIONS
figure;
plot(xx,u_exact(xx),'-k');
hold on;
plot(xx,u_num,'--k');

% ADJUST AXIS FOR SEMI_INFINITE
if (dom == 1)
   % axis([0 15 -0.5 1]);
end