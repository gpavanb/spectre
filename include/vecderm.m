% ENSURE x ARE LGL/LGR POINTS
% COULD HAVE BEEN GENERATED BUT WHY RECOMPUTE?

function c = vecderm(n,m,x,vec,phiVec)

global dom;

% COMPUTE DIFFERENTIATION MATRIX
if (dom == 0)
    D = legslbdiff(length(x),x);
elseif (dom == 1)
    D = lafgsrddiff(length(x),x);
end
    
c = vec'.*phiVec';

for i = 1:m
    c = D*c;
end

end