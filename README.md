# SPECTRE #

SPECTRE is written in MATLAB and is a one-dimensional Spectral-Galerkin code which solves any even-order (E in SPECTRE) linear differential equation in O(N) time using efficient Spectral-Galerkin schemes. 

## How do I get set up? ##

Just modify the input section in `testSolution.m`

* `dom` denotes domain type. Choose 0 for finite and 1 for semi-infinite
* `Ngrid` denotes the number of grid points in each dimension
* `N` denotes the number of basis functions used for solving the differential equation
* `rhs` denotes the source term, which needs to be input as an inline function
* `aMat` is the matrix which denotes the coefficients of the differential equation at quadrature points 

The general form of the linear differential operator is $\\sum\_{i=1}^{2m+1} bVec\_{i}\\partial^{(2m+1)-i}$

## License ##

Please refer to the LICENSE.pdf in the repository. Note that this code requires PRIOR PERMISSION FROM AUTHORS FOR COMMERCIAL PURPOSES.

## Who do I talk to? ##

* Repo owner or admin : [Pavan Bharadwaj](https://bitbucket.org/gpavanb)
* Other community or team contact : The code was developed at the Flow Physics and Computational Engineering group at Stanford University. Please direct any official queries to [Prof. Matthias Ihme](mailto:mihme@stanford.edu)


## References ##

[1] Ben-Yu Guo, Jie Shen, and Li-Lian Wang. 2006. Optimal Spectral-Galerkin Methods Using Generalized Jacobi Polynomials. J. Sci. Comput. 27, 1-3 (June 2006), 305-322. [DOI][http://dx.doi.org/10.1007/s10915-005-9055-7]

[2] Shen, J. (2000). Stable and efficient spectral methods in unbounded domains using Laguerre functions. SIAM Journal on Numerical Analysis, 38(4), 1113-1133.
